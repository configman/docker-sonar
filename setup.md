# mysql

mysqld folder must be readable and writable by the user in the container.  Various options for this on the internet seemed not to work doing in the container so found that simply chmod -R 777 on mysqld folder in volume where mounted worked.

Users were not allowed to connect except from local host.  simplest fix was to run the mysql container and then exec -it bash to get into it.  from there run mysql -u root -p and ...

grant all privileges on *.* to 'root'@'%';

create user 'sonar'@'%' identified by 'sonar'
create database 'sonar';
grant all privileges on 'sonar'.* to 'sonar'@'%';
